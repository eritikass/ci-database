const getData = require("./getData");

it("getData id=1", (done) => {
    getData(1, (result) => {
        expect(result).toMatchSnapshot();
        done();
    })
})